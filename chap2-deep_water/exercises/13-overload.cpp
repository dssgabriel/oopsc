#include <array>
#include <vector>
#include <iostream>

template <typename Container>
std::ostream& _printContainer(std::ostream& os, Container const& container) {
  for (auto val : container){
    os << val;
    os << " ";
  }
  os << std::endl;
  return os;
}

std::ostream& operator<< (std::ostream& os, std::vector<int> const& container){
 return _printContainer(os,container); 
}

std::ostream& operator<< (std::ostream& os, std::array<int,4> const& container){
 return _printContainer(os,container);
}



int main() {
  std::vector vec {0,1,2}; // deduced to be std::vector<int>
  for (auto& val : vec) { // val is a ref toward a vec element
    val += 1;
  }
  std::cout << vec; // overload operator << for std::vector<int>
  std::array static_vec {3,4,5,6}; // deduced to be std::array<int,4>
  for (auto& val : static_vec) { // val is a ref toward a vec element
    val *= 2;
  }
  std::cout << static_vec; // overload operator << for std::array<int,4>
  
}