#include <iostream> 
#include <string>

struct Parent{
    int v;
    void print_parent(){
        std::cout << "Parent ; v = " << v << std::endl;
    }
};

struct Son : protected Parent {
    void print() {
        std::cout << "The Child, ";
        print_parent();
    }
};

struct Son2 : private Parent {
};


int main(){
  Parent p{42};
  p.print_parent(); // output : "Parent ; v = 42"
  std::cout << "parent value " << p.v << std::endl;
  Son s{};
  s.print(); // output "The Child, Parent ; v =42"
  // The following lines must not compile
  //s.print_parent(); // ERROR
  //s.v(); // ERROR
}