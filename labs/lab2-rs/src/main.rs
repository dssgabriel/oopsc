mod equation;
mod problem;
mod time_disc;

use equation::*;
use problem::*;
use time_disc::*;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    assert_eq!(args.len(), 8);

    let a = args[1].parse::<f64>().unwrap();
    let b = args[2].parse::<f64>().unwrap();
    let c = args[3].parse::<f64>().unwrap();
    let x = args[4].parse::<f64>().unwrap();
    let start = args[5].parse::<f64>().unwrap();
    let end = args[6].parse::<f64>().unwrap();
    let step = args[7].parse::<f64>().unwrap();

    let mut p = Problem::new(Equation::new(a, b, c, x), Uniform::new(start, end, step));
    p.solve();
}
