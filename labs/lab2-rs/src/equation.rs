#[derive(Debug)]
pub struct Equation {
    a: f64,
    b: f64,
    c: f64,
    x: f64,
}

impl Equation {
    pub fn new(a: f64, b: f64, c: f64, x: f64) -> Self {
        Equation { a, b, c, x }
    }

    pub fn compute(&mut self, step: f64) {
        if self.a != 0.0 {
            self.x -= (step / self.a) * (self.b * self.x + self.c);
        } else {
            self.x -= self.b * self.x + self.c;
        }
    }

    pub fn x(&self) -> &f64 {
        &self.x
    }
}
