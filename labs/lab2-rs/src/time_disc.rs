pub trait TimeDiscretization {
    fn start(&self) -> f64;
    fn end(&self) -> f64;
    fn step(&self) -> f64;
}

#[derive(Debug)]
pub struct Uniform {
    start: f64,
    end: f64,
    step: f64,
}

impl Uniform {
    pub fn new(start: f64, end: f64, step: f64) -> Self {
        Uniform { start, end, step }
    }
}

impl TimeDiscretization for Uniform {
    fn start(&self) -> f64 {
        self.start
    }

    fn end(&self) -> f64 {
        self.end
    }

    fn step(&self) -> f64 {
        self.step
    }
}
