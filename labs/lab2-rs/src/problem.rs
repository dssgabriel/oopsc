use crate::{equation::Equation, time_disc::TimeDiscretization};

#[derive(Debug)]
pub struct Problem<D: TimeDiscretization> {
    eq: Equation,
    disc: D,
}

impl<D> Problem<D>
where
    D: TimeDiscretization,
{
    pub fn new(eq: Equation, disc: D) -> Self {
        Problem { eq, disc }
    }

    pub fn solve(&mut self) {
        let mut i = self.disc.start();
        while i < self.disc.end() {
            // println!("{:.3}\t{:.6}", i, self.e.x());
            self.eq.compute(self.disc.step());
            i += self.disc.step();
        }
    }
}
