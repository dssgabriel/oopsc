#include <iostream>

#include "equation.hpp"

Equation::Equation(const double a, const double b, const double c)
    : m_a(a), m_b(b), m_c(c) {}

Equation::~Equation() {}

double Equation::initial_condition(const double& t) const
{
    return t;
}

double Equation::compute(const double& t, const double& h, const double& var) const
{
    return t * h + var;
}

template<class T>
double Equation::compute_by_integrator(double& h,
                                       const double& t,
                                       Variable& var) const
{
    // DerivedIntegrator<T, Integrator>();
    return T::update(h, t, var, [](double t) { return t; });
}
