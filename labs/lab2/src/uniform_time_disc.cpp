#include "uniform_time_disc.hpp"

UniformTimeDisc::UniformTimeDisc(const double start,
                                 const double end,
                                 const double step)
    : m_start(start), m_end(end), m_step(step) {}

UniformTimeDisc::~UniformTimeDisc() {}

const double& UniformTimeDisc::start() const
{
    return m_start;
}

const double& UniformTimeDisc::end() const
{
    return m_end;
}

const double& UniformTimeDisc::step() const
{
    return m_step;
}

size_t UniformTimeDisc::nb_points() const
{
    return (size_t)((m_end - m_start) / m_step) + 1;
}

size_t UniformTimeDisc::iteration(const double& t) const
{
    return (size_t)round((t - m_start) / m_step);
}
