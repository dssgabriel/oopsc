#include "integrator.hpp"

void EulerIntegrator::update(double& current_time,
                             const double& time_step,
                             Variable& var,
                             std::function<double(double t)>& fn)
{
    var(current_time + time_step) = fn(current_time) * time_step + var(current_time);
}

void RungeKuttaIntegrator::update(double& current_time,
                                  const double& time_step,
                                  Variable& var,
                                  std::function<double(double t)>& fn)
{
    double k = fn(current_time + (time_step / 2.0f));
    var(current_time + time_step) = time_step * k + var(current_time);
}
