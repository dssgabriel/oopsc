#include <iostream>

#include "timer.hpp"

Timer::Timer()
    : m_duration(0.0f)
{
    m_start = std::chrono::high_resolution_clock::now();
    m_stop = std::chrono::high_resolution_clock::now();
}

Timer::~Timer() {}

void Timer::start() 
{
    m_start = std::chrono::high_resolution_clock::now();
}

void Timer::stop()
{
    m_stop = std::chrono::high_resolution_clock::now();
    m_duration = m_stop - m_start;
}

std::chrono::duration<double> Timer::duration() const
{
    if (m_duration == std::chrono::duration<double>(0.0f)) {
        std::chrono::time_point current = std::chrono::high_resolution_clock::now();
        return current - m_start;
    }

    return m_stop - m_start;
}

void Timer::print() const
{
    std::cout << duration().count() << "s" << std::endl;
}
