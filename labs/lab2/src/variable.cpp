#include <cstdlib>
#include <fstream>
#include <iostream>

#include "variable.hpp"

Variable::Variable(std::shared_ptr<TimeDisc> disc)
    : m_disc(disc)
{
    m_values.resize(m_disc->nb_points());
}

Variable::~Variable()
{
    m_values.clear();
}

void Variable::print(bool to_stdout) const
{
    std::ofstream fp("output.dat", std::ios::out | std::ios::binary | std::ios::trunc);

    for (size_t i = 0; i < m_values.size(); i++) {
        if (to_stdout)
            std::cout << "m_values[" << i << "] = " << m_values[i] << std::endl;
        fp << "m_values[" << i << "] = " << m_values[i] << std::endl;
    }

    fp.close();
}

void Variable::print(const std::string& filename, bool to_stdout) const
{
    std::ofstream fp(filename, std::ios::out | std::ios::binary | std::ios::trunc);

    for (size_t i = 0; i < m_values.size(); i++) {
        if (to_stdout)
            std::cout << "m_values[" << i << "] = " << m_values[i] << std::endl;
        fp << "m_values[" << i << "] = " << m_values[i] << std::endl;
    }

    fp.close();
}

double& Variable::operator()(double t)
{
    size_t n = m_disc->iteration(t);
    if (n >= m_values.size()) {
        std::cout << "Error: " << n << " is out of bounds" << std::endl;
        std::exit(EXIT_FAILURE);
    }
    return m_values[n];
}

const double& Variable::operator()(double t) const
{
    size_t n = m_disc->iteration(t);
    if (n >= m_values.size()) {
        std::cout << "Error: " << n << " is out of bounds" << std::endl;
        std::exit(EXIT_FAILURE);
    }
    return m_values[n];
}
