#include <cstddef>
#include <iostream>
#include <thread>

#include "problem.hpp"
#include "timer.hpp"
#include "variable.hpp"

Problem::Problem(Equation eq, std::shared_ptr<TimeDisc> disc)
    : m_eq(eq)
{
    m_disc = std::move(disc);
}

Problem::~Problem() {}

void Problem::solve()
{
    Variable approx(m_disc);
    // Variable euler(m_disc);
    // Variable runge_kutta(m_disc);
    Variable exact(m_disc);

    approx(0.0) = m_eq.initial_condition(m_disc->start());
    // euler(0.0) = m_eq.initial_condition(m_disc->start());
    // runge_kutta(0.0) = m_eq.initial_condition(m_disc->start());
    exact(0.0) = m_eq.initial_condition(m_disc->start());

    auto lambda = [exact](auto t){ return (t * t / 2) + exact(0.0); };

    Timer loop_timer;
    loop_timer.start();
    for (double i = m_disc->start();
         i < m_disc->end() - m_disc->step();
         i += m_disc->step())
    {
        approx(i + m_disc->step()) = m_eq.compute(i, m_disc->step(), approx(i));
        // euler(i + m_disc->step()) = m_eq.compute_by_integrator<EulerIntegrator>(
        //     i, m_disc->step(), euler);
        // runge_kutta(i + m_disc->step()) = m_eq.compute_by_integrator<RungeKuttaIntegrator>(
        //     i, m_disc->step(), runge_kutta);
        exact(i + m_disc->step()) = lambda(i);
    }

    approx.print("approx.dat", false);
    // euler.print("euler.dat", false);
    // runge_kutta.print("runge_kutta.dat", false);
    exact.print("exact.dat", false);
    loop_timer.stop();

    std::cout << "Approximate: " << (m_disc->end() - m_disc->step()) << " = "
              << approx(m_disc->end() - m_disc->step()) << std::endl;
    // std::cout << "Euler: " << (m_disc->end() - m_disc->step()) << " = "
    //           << euler(m_disc->end() - m_disc->step()) << std::endl;
    // std::cout << "Runge-Kutta: " << (m_disc->end() - m_disc->step()) << " = "
    //           << runge_kutta(m_disc->end() - m_disc->step()) << std::endl;
    std::cout << "Exact: " << (m_disc->end() - m_disc->step()) << " = "
              << exact(m_disc->end() - m_disc->step()) << std::endl;

    std::cout << "Solve time: ";
    loop_timer.print();
}

void Problem::parallel_solve()
{
    Timer in;
    Variable approx(m_disc);
    Variable exact(m_disc);

    auto closure = [exact](auto t){ return (t * t / 2) + exact(0.0); };

    approx(0.0) = m_eq.initial_condition(m_disc->start());
    exact(0.0) = m_eq.initial_condition(m_disc->start());

    in.start();
    std::thread approx_solve([this, &approx] {
        for (double i = m_disc->start(); i < m_disc->end() - m_disc->step(); i += m_disc->step())
            approx(i + m_disc->step()) = m_eq.compute(i, m_disc->step(), approx(i));
        approx.print("parallel_approx.dat", false);
    });

    std::thread exact_solve([this, &closure, &exact] {
        for (double i = m_disc->start(); i < m_disc->end() - m_disc->step(); i += m_disc->step())
            exact(i + m_disc->step()) = closure(i);
        exact.print("parallel_exact.dat", false);
    });
    approx_solve.join();
    exact_solve.join();
    in.stop();

    std::cout << "Approximate: " << (m_disc->end() - m_disc->step()) << " = "
              << approx(m_disc->end() - m_disc->step()) << std::endl;
    std::cout << "Exact: " << (m_disc->end() - m_disc->step()) << " = "
              << exact(m_disc->end() - m_disc->step()) << std::endl;

    std::cout << "Solve time: ";
    in.print();
}
