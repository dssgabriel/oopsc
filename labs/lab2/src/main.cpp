#include <iostream>
#include <memory>

#include "problem.hpp"
#include "timer.hpp"
#include "uniform_time_disc.hpp"

int main(int argc, char **argv)
{
    Timer out;
    out.start();
    
    if (argc != 7) {
        std::cout << "Usage: " << argv[0] 
                  << " a b c start end step" << std::endl; 
        return EXIT_FAILURE;
    }

    double a = atof(argv[1]);
    double b = atof(argv[2]);
    double c = atof(argv[3]);
    double start = atof(argv[4]);
    double end = atof(argv[5]);
    double step = atof(argv[6]);

    Problem p(Equation(a, b, c),
              std::make_shared<UniformTimeDisc>(start, end, step));

    std::cout << "-- Solving problem sequentially --" << std::endl;
    p.solve();
    // std::cout << "-- Solving problem in parallel --" << std::endl;
    // p.parallel_solve();

    out.stop();
    std::cout << "Total time: ";
    out.print();

    return 0;
}
