# POOCS - Ordinary Differential Equation solver
### Lab 2 - CMake and setup
### Mandatory functionalities
- [x] Simulator structure
- [x] Time discretization
- [x] CMake compilation
- [x] Testing with CTest

### Bonus functionalities
- [x] Testing with GTest
- [x] Dynamic choice of time discretization

---

## Lab 3 - Solver and equation
### Mandatory functionalities
- [x] Implementation of the `Variable` class
- [x] Computing initial condition
- [x] Integrating the equation with the `Equation` class

### Bonus functionalities
- [x] New integrators (partially done, code is implemented but has a linking error)
- [ ] Solve muliple equations
- [ ] Changing the time discretization (only uniform time discretization is implemented)
- [ ] Making the code more robust with concepts

---

## Lab 4 - Validation and performance
### Mandatory functionalities
- [x] Validating results (display + file output)
- [x] Computing the exact solution with lambda expressions
- [x] Analyzing performance by implementing the `Timer` class
- [x] Improving performance with `std::threads`

### Bonus functionalities
- [x] Parallelizing the loop computing the exact solution (partially done, not implemented with TBB)
- [ ] Replacing `std::thread` with `std::async`

---

## Lab5 - Performance portability
### Bonus functionalities
- [ ] Installing and compiling the Kokkos library
- [ ] Loading Kokkos in the code
- [ ] Activating multi-threading with Kokkos
- [ ] Activating CUDA with Kokkos
