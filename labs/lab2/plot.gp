set term pngcairo size 1080, 720
set output "plot.png"
set grid
set logscale y
plot "cpp.dat" w l t "C++ f(x) = dx - x", exp(x) t "Gnuplot f(x) = e^x"
