#include <iostream>
#include <memory>

#include "gtest/gtest.h"
#include "problem.hpp"
#include "timer.hpp"
#include "uniform_time_disc.hpp"

TEST(UnitTest, sequential_solve)
{
    Timer out;
    out.start();
    
    double a = 0.0f;
    double b = 0.0f;
    double c = 0.0f;
    double start = 0.0f;
    double end = 100.0f;
    double step = 0.0001f;

    Problem p(Equation(a, b, c),
              std::make_shared<UniformTimeDisc>(start, end, step));

    std::cout << "-- Solving problem sequentially --" << std::endl;
    p.solve();

    out.stop();
    std::cout << "Total time: ";
    out.print();
}

TEST(UnitTest, parallel_solve)
{
    Timer out;
    out.start();
    
    double a = 0.0f;
    double b = 0.0f;
    double c = 0.0f;
    double start = 0.0f;
    double end = 100.0f;
    double step = 0.0001f;

    Problem p(Equation(a, b, c),
              std::make_shared<UniformTimeDisc>(start, end, step));

    std::cout << "-- Solving problem in parallel --" << std::endl;
    p.parallel_solve();

    out.stop();
    std::cout << "Total time: ";
    out.print();
}
