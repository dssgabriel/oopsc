#pragma once

#include <functional>

#include "variable.hpp"

class Integrator {
public:
    Integrator() = delete;
};

class EulerIntegrator: public Integrator {
public:
    EulerIntegrator() = delete;
    static void update(double& current_time,
                       const double& time_step,
                       Variable& var,
                       std::function<double(double t)>& fn); 
};

class RungeKuttaIntegrator: public Integrator {
public:
    RungeKuttaIntegrator() = delete;
    static void update(double& current_time,
                       const double& time_step,
                       Variable& var,
                       std::function<double(double t)>& fn); 
};

// template<class T, class B>
// struct DerivedIntegrator {
//     static void constraints(T* p) { B* pb = p; }
//     DerivedIntegrator() { void(*p)(T*) = constraints; }
// };
