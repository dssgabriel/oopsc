#pragma once

#include <memory>
#include <ostream>

#include "equation.hpp"
#include "time_disc.hpp"

class Problem {
public:
    Problem(Equation eq, std::shared_ptr<TimeDisc> disc);
    ~Problem();

    void solve();
    void parallel_solve();

private:
    Equation m_eq;
    std::shared_ptr<TimeDisc> m_disc;
};
