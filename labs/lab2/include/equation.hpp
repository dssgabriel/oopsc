#pragma once

#include <ostream>
#include <type_traits>

#include "integrator.hpp"

class Equation {
public:
    Equation(const double a, const double b, const double c);
    Equation(const Equation& other) = default;
    ~Equation();

    double initial_condition(const double& t) const;
    double compute(const double& t, const double& h, const double& var) const;
    friend std::ostream& operator<<(std::ostream& os, Equation& e);

    template<class T>
    double compute_by_integrator(double& h,
                                 const double& t,
                                 Variable& var) const;

private:
    double m_a;
    double m_b;
    double m_c;
};
