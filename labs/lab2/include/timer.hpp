#pragma once

#include <chrono>

class Timer {
public:
    Timer();
    ~Timer();

    void start();
    void stop();
    std::chrono::duration<double> duration() const;
    void print() const;

private:
    std::chrono::time_point<std::chrono::high_resolution_clock> m_start;
    std::chrono::time_point<std::chrono::high_resolution_clock> m_stop;
    std::chrono::duration<double> m_duration;
};
