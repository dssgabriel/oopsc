#pragma once

#include <cstddef>

class TimeDisc {
public:
    virtual ~TimeDisc() = default;

    virtual const double& start() const = 0;
    virtual const double& end() const = 0;
    virtual const double& step() const = 0;
    virtual size_t nb_points() const = 0;
    virtual size_t iteration(const double& t) const = 0;
};
