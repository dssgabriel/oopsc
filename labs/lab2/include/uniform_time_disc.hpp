#pragma once

#include <cmath>

#include "time_disc.hpp"

class UniformTimeDisc: public TimeDisc {
public:
    UniformTimeDisc(const double start, const double end, const double step);
    ~UniformTimeDisc();

    const double& start() const override;
    const double& end() const override;
    const double& step() const override;
    size_t nb_points() const override;
    size_t iteration(const double& t) const override;

private:
    double m_start;
    double m_end;
    double m_step;
};
