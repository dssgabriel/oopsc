#pragma once

#include <cstddef>
#include <memory>
#include <string>
#include <vector>

#include "time_disc.hpp"

class Variable {
public:
    Variable(std::shared_ptr<TimeDisc> disc);
    ~Variable();

    void print(bool to_stdout) const;
    void print(const std::string& filename, bool to_stdout) const;

    double& operator()(double t);
    const double& operator()(double t) const;

private:
    std::vector<double> m_values;
    std::shared_ptr<TimeDisc> m_disc;
};
