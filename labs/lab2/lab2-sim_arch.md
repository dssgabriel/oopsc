# TD 2 à 4 Prévoir la pollution atmosphérique : résolution d'équations différentielles ordinaires

Contexte : il est aujourd'hui primordial de controler la quantité de polluants dans l'air de nos grandes agglomérations ainsi que son évolution. Ces polluants sont par exemple produits lors de la combustion d'hydrocarbures d'origine fossile. Cette combustion met en jeu des dizaines voire des centaines de réactions chimiques. L'évolution de la quantité des espèces chimiques en présence est régie par des équations différentielles ordinaires, qu'il va falloir résoudre numériquement pour espérer prédire les quantités de polluants produits. Nous nous proposons de réaliser dans ces séances de travaux dirigés un solveur d'ODE (ordinary differential equations) du premier ordre. Ce solveur devra in fine être capable de résoudre plusieurs types d'équations et de proposer un catalogue de méthodes de résolution. 

Nous proposons d'utiliser ce contexte comme fil rouge de nos séances de TD.

# TD 2 Mise en place de l'infrastructure

## 2.1 Structure du simulateur
Le simulateur sera constitué de deux classes principales  
- Une classe `Problem` permettant de résoudre nos équations,
- une classe `Equation`, contenant les paramètres de notre équation à résoudre.

Vous créerez des fichiers .cpp et .h pour chacune de ces classes.
La classe `Problem` contiendra un membre de type `Equation`.

Le programme principal se limitera à créer une instance de la classe `Problem` et à appeler sa méthode `solve`. Cette méthode appelle la méthode `compute` de l'`Equation` contenue dans le `Problem`.

Le programme sera configuré par un projet CMake, voir partie 2.3. 

## 2.2 Discrétisation en temps

Nous allons maintenant créer l'outillage pour la discrétisation en temps. Afin de permettre de la souplesse dans le choix de cette discrétisation (uniforme ou non par exemple), nous allons avoir recours au polymorphisme. Nous utiliserons une classe de base `ITimeDiscretization` qui proposera des méthodes pour accéder au temps initial, final et au pas de temps. Vous créerez cette classe de base ainsi qu'une classe dérivée `UniformTimeDiscretization` qui implémentera les méthodes de la classe parent pour un pas constant.

Vous stockerez un pointeur de `ITimeDiscretization` dans `Problem` et vous l'utiliserez pour mettre au point une boucle en temps dans la méthode `solve` qui appellera la méthode `compute` d'`Equation` à chaque pas de temps. Pour l'instant, la méthode `solve` se contentera d'afficher `compute equation at time : t` :
```
--- Solve problem ---
--- compute equation at time : 0 ---
--- compute equation at time : 1 ---
--- compute equation at time : 2 ---
--- compute equation at time : 3 ---
--- compute equation at time : 4 ---
```
Pour créer notre `Problem` dans le programme principal, nous lui passerons à la construction une instance de la classe équation et un pointeur de `UniformTimeDiscretization` qu'il stockera dans son pointeur de `ITimeDiscretization`. Il sera responsable de la mémoire de ce pointeur. Vous veillerez à ce que le pointeur ne puisse pas être nul, en bloquant les constructions ou affectations par copie.


## 2.2 Compiler avec CMake

La compilation du projet aura lieu grâce à un projet CMake qui créera une target de type exécutable qui compilera votre main en utilisant toutes les classe que vous aurez définies. Pour compiler votre projet depuis vos sources, il faudra simplement exécuter les commandes suivantes : 
```
mkdir build
cd build
cmake .. 
make
```

## 2.3 Tester avec CTest

Grâce à la commande `add_test(test_name target_name)`, vous ajouterez votre exécutable à la base de test. Vous n'oublierez pas la commande `enable_testing` avant d'ajouter `add_test` dans le CMakeLists.txt.

Vous pouvez maintenant lancer votre exécutable via la commande ctest: 
```
$ ctest
    Start 1: structure_test
1/1 Test #1: structure_test ...................   Passed    0.01 sec

100% tests passed, 0 tests failed out of 1
```

### Bonus : créez des tests unitaires avec GTest

Vous avez créé un test d'intégration (qui utilise tous les éléments de votre projet), il est également nécessaire de créer des test unitaires pour chaque brique (classe) du projet. GTest est un bon outil pour les réaliser. 

- Vous créerez un nouvel exécutable de test, utilisant GTest (revoir TD1), qui vous permettra, à l'aide de la macro `TEST`, de tester vos différentes classes. 
- Grâce à la commande CMake `gtest_discover_tests`, prenant en argument le nom de la target exécutable que vous venez de créer, ces tests seront lancés également via la commande `ctest`.


```c++
TEST(UnitaryTest,ProblemTest){
  // construction et test d'une instance de Problem
}

TEST(MainFunctionTest,EquationTest){
  // construction et test d'une instance de Equation
}
...
```

  

## 2.4 Bonus : choix dynamique de la discrétisation en temps

- Afin de pouvoir choisir le type de discrétisation au runtime, prenez en argument d'entrée le type de discrétisation en temps (via une chaîne de caractère). Vous utiliserez ensuite une fabrique, qui en prenant cette argument d'entrée sera capable de créer l'instance souhaitée de la classe `Problem`. Cette fabrique utilisera une `enum class` définissant les types de discrétisation en temps possible (à ce stade uniquement une discrétisation uniforme) et assurera grâce à un dictionnaire la conversion entre la chaine de caractère donnée en entrée et l'enum.

Le livrable de ce TD doit être configurable via CMake, compilable et avoir une base de tests ok.
