# Introduction à CMake

## Compiler un programme et une bibliothèque

**Exo** : compiler un programme utilisant une classe MyClass située dans une bibliothèque externe.
Pour cela vous devrez : 
- créer un fichier main.cpp qui servira pour tester votre bibliothèque. Vous instantierez la classe MyClass de votre bibliothèque et vous appellerez sa méthode `print`, qui affichera une description de cette classe.
- créer un répertoire mylib qui contiendra un `.h` et un `.cpp`. Ces fichiers vous permettront de définir la classe MyClass ; celle-ci devra appartenir au namespace MyLib. La classe contiendra au moins une méthode `print` que vous déclarerez dans le .h (n'oubliez pas les *include gards*) et que vous définirez dans le `.cpp`.


### Etape 1 : à la main

- Compiler la bibliothèque en dynamique : dans le répertoire mylib 

`g++ -o libmylib.so -fpic -shared mylib.cpp`

- Compiler le programme
  - naïvement `g++ main.cpp`
  - utilisez -L pour le chemin de la bibliothèque, -I pour le chemin d'include et -lnomdelalib (sans le prefix lib ni l'extension .so) pour faire fonctionner la compilation

 `g++ -Lmylib_dir -Imylib_dir main.cpp -lmylib`
 
 En cas d'échec, essayer `-L mylib_dir` (avec un espace).

- Executez le programme
  - naïvement
  - en enrichissant le LD_LIBRARY_PATH où doivent se trouver tous les chemins des bibliothèques
solution : enrichir à la volée : 
`env LD_LIBRARY_PATH=./mylib ./a.out` ou en bash : `LD_LIBRARY_PATH=./mylib ./a.out`

### Etape 2 : avec CMake
- Un tutorial CMake est disponible sur la page : https://cmake.org/cmake/help/latest/guide/tutorial/index.html
- Créez un CMakeLists.txt à côté du `main.cpp`
```cmake
cmake_minimum_required(VERSION 3.10)

# set the project name
project(Tutorial)

# enable modern C++
set(CMAKE_CXX_STANDARD 17)

# add the executable
add_executable(Main.exe main.cpp)
```

- Lancez la configuration : depuis un répertoire build, créé au même niveau que `main.cpp` lancez
`cmake ..` ; CMake génère alors un makefile. Essayez de compiler avec : `make` ou `cmake --build .`
  - la compilation échoue car nous n'avons pas ni 
    - créé la bibliothèque mylib ;
    - indiqué à CMake la dépendance de `main.cpp` à mylib
- En suivant le tutorial CMake, réalisez ces deux étapes, en utilisant les commandes CMake : 
  - `add_library` dans un nouveau `CMakeLists.txt` dans le répertoire mylib
  - `add_subdirectory` et `target_include_directories` dans le `CMakeLists.txt` racine.
- Ajouter un test grâce à la commande `add_test` prenant comme argument votre target `Main.exe`. Cette commande doit être précédée de la commande `enable_testing()` (sans argument). Vous pourrez alors tester votre code via la commande `ctest`.

### Etape 3 : ajouter une dépendance à GTest
#### 3.1 : compiler et installer GTest
  - GTest est déjà installé sur votre machine : vous pouvez passer au 3.2. Vous ne savez pas ?  Demandez à CMake ! voir 3.2...
  - Vous avez les droits sudo pour faire une installation via le système de paquet de votre distribution :  
    - `sudo apt-get install libgtest-dev`
  - Si aucune de ces options ne fonctionne, suivez le *tutorial help install gtest* fourni dans le même répertoire que ce TP.

#### 3.2 : trouver et utiliser GTest via CMake
- La doc CMake est là : 
  - https://cmake.org/cmake/help/v3.18/module/FindGTest.html?highlight=gtest#module:FindGTest
- utilisez la commande `find_package` de CMake pour chercher GTest. Indiquez l'option `REQUIRED`.

### Etape 4 : ajouter des tests via gtest
- Créez un nouveau fichier test.cpp.
- Créez une nouvelle target `Test.exe` de type `executable` contenant test.cpp 
- utilisez la commande `target_link_libraries` pour lier votre target `Test.exe` aux deux targets de GTest : `GTest::GTest`et `GTest::Main` et à votre target `mylib`. N'oubliez pas non plus le `target_include_directories` pour pouvoir utiliser les header de mylib dans votre fichier de test.
- Créez un test de la méthode print de mylib en utilisant les macros de Gtest : 
```c++
#include "gtest/gtest.h"
#include "mylib.h"

// This macro creates a test case MylibTestPrintTest, of type UnitTest
TEST(UnitTest, MylibTestPrintTest){ 
  
  // Create an instance of Mylib::MyClass and test print method
}
```
- Ajoutez une méthode value dans MyClass et créez un nouveau test pour la valider. Par exemple une méthode retournant un entier que vous pourrez alors tester avec la macro gtest : `EXPECT_EQ(val1,val2)`
- Pour que ces tests soient utilisables depuis la commande ctest dans votre répertoire de build, vous pouvez ajouter la commande
```cmake
# Fichier CMakeLists.txt
gtest_discover_tests(Test.exe)
```
Ainsi votre projet sera compilable et testable avec les commandes (classiques) suivantes : 
```
cmake ..
make 
ctest
```

### Etape 5 : exporter les symboles (BONUS)
Afin de vous assurer que votre code est bien cross-compilable (ie compilable sur d'autres plateformes), il est utilse de désactiver l'export automatique de symboles, car celui-ci n'est pas actif sous Windows. Pour cela utiliser l'option de gcc `-fvisibility=hidden`.
- Essayer de compiler votre code avec cette option 
  - pour la passer cette option à CMake, utilisez l'option `CMAKE_CXX_FLAGS`
- Corriger le code de votre bibliothèque pour réparer l'édition des liens.

### Etape 6 : installer votre code (BONUS 2)
S'il vous reste du temps mettez en place l'installation de votre bibliothèque dans le CMake, de sorte qu'un simple find_package(my_lib) dans un code utilisateur permette d'utiliser votre bibliothèque.